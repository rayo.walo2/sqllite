package com.example.labo3;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        dbWorker dbw = new dbWorker(this);
        dbw.execute();
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_account:
                startActivity(new Intent(this, add_account.class));
                return true;
            case R.id.delete_account:
                startActivity(new Intent(this, delete_account.class));
                return true;
            case R.id.edit_account:
                startActivity(new Intent(this, edit_account.class));
                return true;
            case R.id.view_account:
                startActivity(new Intent(this, view_account.class));
                return true;
            case R.id.credit_account:
                startActivity(new Intent(this, credit_account.class));
                return true;
            case R.id.edit_account_credit:
                startActivity(new Intent(this, edit_account_credit.class));
                return true;
            case R.id.edit_account_balance:
                startActivity(new Intent(this, edit_account_balance.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public class dbWorker extends AsyncTask {
        private Context c;

        public dbWorker(Context c) {
            this.c = c;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected ArrayList<Client> doInBackground(Object[] param) {
            MyDBAdapter db = new MyDBAdapter(c);
            db.open();
            ArrayList<Client> lc;
            lc = db.selectAllClients();
            return lc;
        }

        @Override
        protected void onPostExecute(Object o) {
            final ArrayList<Client> l = (ArrayList<Client>) o;
            final ArrayList<String> names = new ArrayList<>();
            for (Client c : l) {
                names.add(c.lastname+" "+c.firstname);
            }
            ListView listView = findViewById(R.id.list);
            ArrayAdapter<String> itemsAdapter = new ArrayAdapter<String>(c, android.R.layout.simple_list_item_1, names);
            listView.setAdapter(itemsAdapter);

        }
    }
}
