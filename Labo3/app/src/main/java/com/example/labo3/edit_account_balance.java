package com.example.labo3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class edit_account_balance extends AppCompatActivity {
    EditText editText,balance;
    Client c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_account_balance);
        MyDBAdapter db = new MyDBAdapter(this);
        db.open();
        editText = (EditText)findViewById(R.id.clientid);
        balance = (EditText)findViewById(R.id.balance);

        final ArrayList<Client> lc;
        lc = db.selectAllClients();
        final ArrayList<String> names = new ArrayList<>();
        for (Client c : lc) {
            names.add(c.lastname+" "+c.firstname);
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, names);
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                 c = lc.get(position);
                editText.setText(Integer.toString(c.id));
            }
        });
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_account:
                startActivity(new Intent(this, add_account.class));
                return true;
            case R.id.delete_account:
                startActivity(new Intent(this, delete_account.class));
                return true;
            case R.id.edit_account:
                startActivity(new Intent(this, edit_account.class));
                return true;
            case R.id.view_account:
                startActivity(new Intent(this, view_account.class));
                return true;
            case R.id.credit_account:
                startActivity(new Intent(this, credit_account.class));
                return true;
            case R.id.edit_account_credit:
                startActivity(new Intent(this, edit_account_credit.class));
                return true;
            case R.id.edit_account_balance:
                startActivity(new Intent(this, edit_account_balance.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void editBalance(View view)
    {
        dbWorker dbw = new dbWorker(this);
        dbw.execute();
    }
    public class dbWorker extends AsyncTask {
        private Context context;

        public dbWorker(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Client doInBackground(Object[] param) {
            MyDBAdapter db = new MyDBAdapter(context);
            db.open();
            float bl =Float.valueOf(balance.getText().toString());
            String id=editText.getText().toString();
            db.editClientBalance(bl,id);
            return c;
        }

        @Override
        protected void onPostExecute(Object o) {
            Client c = (Client)o;
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Client Updated!!");
            builder.setMessage("The following client balance were updated successfully: "+c.lastname+" "+c.firstname);
            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(context, edit_account_balance.class);
                    context.startActivity(intent);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();
        }
    }
}
