package com.example.labo3;

public class Client {
    public int id;
    public String lastname,  firstname,  address,  username,  password;
    public float balance, credit;
    public Client(int id, String lastname, String firstname, String address, String username, String password, float balance, float credit) {
    this.id=id;
    this.lastname=lastname;
    this.firstname=firstname;
    this.address=address;
    this.username=username;
    this.password=password;
    this.balance=balance;
    this.credit=credit;

    }
}
