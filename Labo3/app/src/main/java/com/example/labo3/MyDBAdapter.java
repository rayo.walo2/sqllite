package com.example.labo3;
import android.content.ContentValues;
import android.content.Context;
import android.database.*;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

public class MyDBAdapter {
    private Context context;
    private String DATABASE_NAME="labo4.db";
    private MyDBHelper dbHelper;
    private int DATABASE_VERSION=1;
    private SQLiteDatabase mySQLiteDatabase;
    public MyDBAdapter(Context context)
    {
        this.context=context;
        dbHelper=new MyDBHelper(context,DATABASE_NAME,null,DATABASE_VERSION);
        //dbHelper.onCreate(mySQLiteDatabase);
    }
    public void open()
    {
        this.mySQLiteDatabase=dbHelper.getWritableDatabase();
    }
    public void insertClient(Client c)
    {
        ContentValues cv = new ContentValues();
        cv.put("lastname",c.lastname);
        cv.put("firstname",c.firstname);
        cv.put("address",c.address);
        cv.put("username",c.username);
        cv.put("password",c.password);
        cv.put("balance",c.balance);
        cv.put("credit",c.credit);
        this.mySQLiteDatabase.insert("clients",null,cv);
    }
    public void editClient(Client c)
    {
        ContentValues cv = new ContentValues();
        cv.put("lastname",c.lastname);
        cv.put("firstname",c.firstname);
        cv.put("address",c.address);
        cv.put("username",c.username);
        cv.put("password",c.password);
        String id=Integer.toString(c.id);
        String[] whereArgs = new String[] { String.valueOf(id) };
        this.mySQLiteDatabase.update("clients", cv, "id=?", whereArgs);
    }
    public void editClientCredit(float credit,String id)
    {
        ContentValues cv = new ContentValues();
        cv.put("credit",credit);
        String[] whereArgs = new String[] { String.valueOf(id) };
        this.mySQLiteDatabase.update("clients", cv, "id=?", whereArgs);
    }
    public void editClientBalance(float balance,String id)
    {
        ContentValues cv = new ContentValues();
        cv.put("balance",balance);
        String[] whereArgs = new String[] { String.valueOf(id) };
        this.mySQLiteDatabase.update("clients", cv, "id=?", whereArgs);
    }
    public void deleteClient(String id)
    {
        String[] whereArgs = new String[] { String.valueOf(id) };
        this.mySQLiteDatabase.delete("clients","id=?",whereArgs);
    }
    public Client getClient(String id)
    {
        Cursor cursor2 = mySQLiteDatabase.rawQuery("SELECT * FROM clients WHERE id = '"+id+"'", null);
        cursor2.moveToFirst();
        Client c= new Client(cursor2.getInt(0),cursor2.getString(1),cursor2.getString(2),cursor2.getString(3),cursor2.getString(4),cursor2.getString(5),cursor2.getFloat(6),cursor2.getFloat(7));

        return c;
    }
    public ArrayList<Client> selectCreditClients()
    {
        ArrayList<Client> CreditClients = new ArrayList<Client>();
        Cursor cursor = mySQLiteDatabase.rawQuery("SELECT * FROM clients WHERE credit > balance;", null);
        if(cursor !=null && cursor.moveToFirst())
        {
            do
            {
                CreditClients.add(new Client(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getFloat(6),cursor.getFloat(7)));
            }while(cursor.moveToNext());
        }
        return CreditClients;
    }
    public ArrayList<Client> selectAllClients()
    {
        ArrayList<Client> allClients = new ArrayList<Client>();
        Cursor cursor = mySQLiteDatabase.query("clients",null,null,null,null,null,null);
        if(cursor !=null && cursor.moveToFirst())
        {
            do
            {
                allClients.add(new Client(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getFloat(6),cursor.getFloat(7)));
            }while(cursor.moveToNext());
        }
        return allClients;
    }
    public void deleteAllClients()
    {
        this.mySQLiteDatabase.delete("clients",null,null);
    }
}
class MyDBHelper extends SQLiteOpenHelper
{

    public MyDBHelper(Context context, String dbname, SQLiteDatabase.CursorFactory factory, int version) {
        super(context,dbname,factory,version);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String query = "CREATE TABLE clients(id integer primary key AUTOINCREMENT ,lastname text,firstname text,address text,username text,password text, balance real,credit real);";
        db.execSQL(query);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        String query ="DROP TABLE IF EXISTS clients;";
        db.execSQL(query);
        onCreate(db);
    }
    @Override
    public void onOpen(SQLiteDatabase db)
    {
        super.onOpen(db);
    }
}