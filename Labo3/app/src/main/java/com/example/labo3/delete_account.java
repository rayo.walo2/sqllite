package com.example.labo3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import java.util.ArrayList;

public class delete_account extends AppCompatActivity {
    EditText editText;
    String id;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_account);
        MyDBAdapter db = new MyDBAdapter(this);
        db.open();
         editText = (EditText)findViewById(R.id.clientid);

        final ArrayList<Client> lc;
        lc = db.selectAllClients();
        final ArrayList<String> names = new ArrayList<>();
        for (Client c : lc) {
            names.add(c.lastname+" "+c.firstname);
        }
        ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(
                this, android.R.layout.simple_spinner_item, names);
        spinnerArrayAdapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
        Spinner spinner = (Spinner)findViewById(R.id.spinner);
        spinner.setAdapter(spinnerArrayAdapter);
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {

            @Override
            public void onNothingSelected(AdapterView<?> parent) {
            }

            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                Client c = lc.get(position);
                editText.setText(Integer.toString(c.id));
            }
        });

    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_account:
                startActivity(new Intent(this, add_account.class));
                return true;
            case R.id.delete_account:
                startActivity(new Intent(this, delete_account.class));
                return true;
            case R.id.edit_account:
                startActivity(new Intent(this, edit_account.class));
                return true;
            case R.id.view_account:
                startActivity(new Intent(this, view_account.class));
                return true;
            case R.id.credit_account:
                startActivity(new Intent(this, credit_account.class));
                return true;
            case R.id.edit_account_credit:
                startActivity(new Intent(this, edit_account_credit.class));
                return true;
            case R.id.edit_account_balance:
                startActivity(new Intent(this, edit_account_balance.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void delete(View view)
    {
         id=this.editText.getText().toString();

        dbWorker dbw = new dbWorker(this);
        dbw.execute();
    }
    public class dbWorker extends AsyncTask {
        private Context context;

        public dbWorker(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected Object doInBackground(Object[] param) {
            MyDBAdapter db = new MyDBAdapter(context);
            db.open();
            db.deleteClient(id);
            return null;
        }

        @Override
        protected void onPostExecute(Object o) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Client deleted!!");
            builder.setMessage("The client was deleted successfully: ");
            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(context, delete_account.class);
                    context.startActivity(intent);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }
    }
}
