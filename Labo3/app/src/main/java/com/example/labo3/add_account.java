package com.example.labo3;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import java.util.ArrayList;

public class add_account extends AppCompatActivity {
    private EditText username,pw,name,lastname,address,balance,credit ;
    private Button add_account_btn;
    private Client c;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_account);
        this.username=(EditText)findViewById(R.id.username);
        this.pw=(EditText)findViewById(R.id.pw);
        this.name=(EditText)findViewById(R.id.firstname);
        this.lastname=(EditText)findViewById(R.id.lastname);
        this.address =(EditText)findViewById(R.id.address );
        this.balance =(EditText)findViewById(R.id.balance );
        this.credit =(EditText)findViewById(R.id.credit );
    }
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.my_options_menu, menu);
        return true;
    }
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.add_account:
                startActivity(new Intent(this, add_account.class));
                return true;
            case R.id.delete_account:
                startActivity(new Intent(this, delete_account.class));
                return true;
            case R.id.edit_account:
                startActivity(new Intent(this, edit_account.class));
                return true;
            case R.id.view_account:
                startActivity(new Intent(this, view_account.class));
                return true;
            case R.id.credit_account:
                startActivity(new Intent(this, credit_account.class));
                return true;
            case R.id.edit_account_credit:
                startActivity(new Intent(this, edit_account_credit.class));
                return true;
            case R.id.edit_account_balance:
                startActivity(new Intent(this, edit_account_balance.class));
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
    public void add(View view)
    {
        String userN=this.username.getText().toString();
        String userP=this.pw.getText().toString();
        String firstname=this.name.getText().toString();
        String lastname=this.lastname.getText().toString();
        String address=this.address.getText().toString();
        String balance=this.balance.getText().toString();
        String credit=this.credit.getText().toString();

        dbWorker dbw = new dbWorker(this);
         c = new Client(0,lastname,firstname,address,userN,userP,Float.valueOf(balance),Float.valueOf(credit));
        dbw.execute();
    }
    public class dbWorker extends AsyncTask {
        private Context context;

        public dbWorker(Context context) {
            this.context = context;
        }

        @Override
        protected void onPreExecute() {

        }

        @Override
        protected ArrayList<Client> doInBackground(Object[] param) {
            MyDBAdapter db = new MyDBAdapter(context);
            db.open();
            db.insertClient(c);
            ArrayList<Client> lc;
            lc = db.selectAllClients();
            return lc;
        }

        @Override
        protected void onPostExecute(Object o) {
            AlertDialog.Builder builder = new AlertDialog.Builder(context);
            builder.setTitle("Client Added!!");
            builder.setMessage("The following client was added successfully: "+c.lastname+" "+c.firstname);
            builder.setPositiveButton("Done", new DialogInterface.OnClickListener() {

                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(context, MainActivity.class);
                    context.startActivity(intent);
                }
            });
            AlertDialog dialog = builder.create();
            dialog.show();


        }
    }
}
